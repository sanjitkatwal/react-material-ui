import { createTheme } from "@material-ui/core";
import { blue, purple } from "@mui/material/colors";

export const theme = createTheme({
    palette:{
        primary:{
            main:purple[500],
        },
    },
});