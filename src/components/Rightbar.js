import {Container, makeStyles} from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
    container:{
        paddingTop:theme.spacing(8),
    }
}));

const Rightbar = () => {
    const classes = useStyles()
    return (
        <Container className={classes.container}>
            <p>this is right bar</p>
        </Container>
    );
}

export default Rightbar;
