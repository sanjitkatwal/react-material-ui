import {AppBar, Avatar, Badge, InputBase, makeStyles, Toolbar, Typography} from '@material-ui/core';
import {Cancel, Mail, Notifications, Search} from "@material-ui/icons";
import {alpha} from "@material-ui/core";
import {useState} from "react";


// const useStyles = makeStyles((theme) => ({}));
const useStyles = makeStyles((theme) => ({
    toolbar:{
        display:'flex',
        justifyContent:'space-between'
    },

    logoLg:{
        display:'none',
        [theme.breakpoints.up('sm')]:{
            display: 'block',
        }
    },
    logoSm:{
        display:'block',
        [theme.breakpoints.up('sm')]:{
            display: 'none',
        }
    },

    search:{
        display:'flex',
        alignItems:'center',
        borderRadius: theme.shape.borderRadius,
        width:'40%',
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        [theme.breakpoints.down('sm')]:{
            display: (props)=>props.open ? 'flex' :'none',
            width: '70%'
        }
    },
    input:{
        color:'white',
        marginLeft:theme.spacing(1)
    },
    cancel:{
        [theme.breakpoints.up('sm')]:{
            display: 'none',
        }
    },
    searchButton:{
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]:{
            display: 'none',
        }
    },
    icons:{
        alignItems:'center',
        display: (props)=>props.open ? 'none' :'flex',
    },
    badge:{
        marginRight: theme.spacing(2),
    },

}));

const Navbar = () => {
  const [open, setOpen] = useState(false);
  const classes = useStyles({open})
  return (
    <AppBar position='fixed'>
        <Toolbar className={classes.toolbar}>
            <Typography variant='h6' className={classes.logoLg}>SK DEV</Typography>
            <Typography variant='h6' className={classes.logoSm}>DEV</Typography>
            <div className={classes.search}>
                <Search />
                <InputBase
                    className={classes.input}
                    placeholder="Search…"
                    inputProps={{ 'aria-label': 'search' }}
                />
                <Cancel className={classes.cancel} onClick={() => setOpen(false)} />
            </div>
           <div className={classes.icons}>
               <Search className={classes.searchButton} onClick={() => setOpen(true)} />
               <Badge color="secondary" badgeContent={99} className={classes.badge}>
                   <Mail />
               </Badge>
               <Badge color="secondary" badgeContent={2} className={classes.badge}>
                   <Notifications />
               </Badge>
               <Avatar alt="Remy Sharp" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNLBZYdauSpXz3JqjsBio6uqE8aZg98Dg1CQ&usqp=CAU" />
           </div>
        </Toolbar>
        </AppBar>
  );
}

export default Navbar;

