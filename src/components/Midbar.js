import {Container, makeStyles} from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
    container:{
        backgroundColor:'black',
        color:'white',
        paddingTop:theme.spacing(8),
    }
}));

const Midbar = () => {
  const classes = useStyles()
  return (
      <Container className={classes.container}>
      <p>lorem4000</p>
      </Container>
  );
}

export default Midbar;
